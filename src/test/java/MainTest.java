import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


// TODO PLEASE SEE ATTACHED TEST CASE IN GENERAL PROJECT FOLDER.


class MainTest {


    @Test
    public void Task1() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        driver.get("https://www.google.com/");

        WebElement searchBar = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));

        searchBar.sendKeys("Selenium" + Keys.ENTER);

        WebElement searchResult = driver.findElement(By.cssSelector("#rso > div > div:nth-child(1) > div > div.yuRUbf > a > div > cite"));

        searchResult.click();

        String expected = "https://www.selenium.dev/";

        assertEquals(expected, driver.getCurrentUrl());

        driver.quit();
    }


    @Test
    public void Task2() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();

        driver.get("https://mail.ru/");

        driver.findElement(By.xpath("//*[@id=\"saveauth\"]")).click(); // removing Save me

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input")).sendKeys("jfkisafk" + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")).sendKeys("2&pipphRIOG3" + Keys.ENTER);

        wait.until(ExpectedConditions.urlToBe("https://e.mail.ru/inbox/?back=1&afterReload=1"));

        String expectedUrl = "https://e.mail.ru/inbox/?back=1&afterReload=1";

        assertEquals(expectedUrl, driver.getCurrentUrl());

        driver.quit();
    }


    @Test
    public void Task3() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        driver.get("https://mail.ru/");

        driver.findElement(By.xpath("//*[@id=\"saveauth\"]")).click(); // removing Save me

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input")).sendKeys("jfkisafk" + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")).sendKeys("olololololo" + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        assertTrue(driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[3]")).isDisplayed());

        driver.quit();

    }

    @Test
    public void Task4() throws IOException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        File file = new File("src\\test\\resources\\qwerty.txt");
        driver.manage().window().maximize();
        driver.get("https://mail.ru/");

        driver.findElement(By.xpath("//*[@id=\"saveauth\"]")).click(); // removing Save me

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[1]/div[2]/input")).sendKeys("jfkisafk" + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")).sendKeys("2&pipphRIOG3" + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"app-canvas\"]/div/div[1]/div[1]/div/div[2]/span/div[1]/div[1]/div/div/div/div[1]/div/div/a/span/span")).click(); // to write a new message

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.className("container--zU301")).sendKeys("justforouter@gmail.com");

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.className("desktopInput--3cWPE")).sendKeys(file.getCanonicalPath()); // attachment

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[2]/div[1]/span[1]/span/span")).click();

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        assertTrue(driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div[2]/div/div/div[2]/a")).isDisplayed()); // verification of successful operation

        driver.quit();
    }

    @Test
    public void Task5() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        Actions hold = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 2);
        driver.get("https://www.etsy.com/ ");

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        hold.moveToElement(driver.findElement(By.xpath("//*[@id=\"catnav-primary-link-10923\"]"))).perform(); // Clothing&Shoes

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"side-nav-category-link-10936\"]"))); // Men's clickable

        hold.moveToElement(driver.findElement(By.xpath("//*[@id=\"side-nav-category-link-10936\"]"))).perform(); // Men's

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"catnav-l4-11109\"]"))); // Boots clickable

        hold.moveToElement(driver.findElement(By.xpath("//*[@id=\"catnav-l4-11109\"]"))).click(driver.findElement(By.xpath("//*[@id=\"catnav-l4-11109\"]"))).perform(); // Boots

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//*[@id=\"search-filter-reset-form\"]/div[2]/fieldset/div/div/div[2]/div/a/label")).click(); // Discount

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        // TODO PLEASE NOTE THAT NUMBER OF GOODS IN DISCOUNT IS DYNAMIC, SO IT WAS IMPOSSIBLE TO ASSERT IT BY EXACT AMOUNT.

        // TODO Tutor is already informed about this situation. In this case assertTrue method()|.line190.| validates that number of goods with "DISCOUNT" mark are showed.

        assertTrue(driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[1]/div/span/span")).isDisplayed()); // number of goods in discount is displayed

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        String result = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[1]/div/span/span/span[2]")).getText();

        System.out.println(result);

        driver.quit();
    }
}


